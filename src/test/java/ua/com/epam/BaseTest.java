package ua.com.epam;

import org.testng.annotations.AfterMethod;
import ua.com.epam.factory.DriverProvider;


public class BaseTest {
    @AfterMethod
    public void tearDown()  {
        DriverProvider.quitDriver();
    }
}
