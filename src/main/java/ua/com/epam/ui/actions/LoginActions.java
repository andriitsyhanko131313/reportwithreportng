package ua.com.epam.ui.actions;

import ua.com.epam.ui.pages.LoginPage;


public class LoginActions {
    private final LoginPage loginPage;

    public LoginActions() {
        loginPage = new LoginPage();
    }

    public void logInToAccount(String login, String password) {
        loginPage.setLoginField(login);
        loginPage.setPasswordField(password);
    }
}
