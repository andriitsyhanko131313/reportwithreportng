package ua.com.epam.ui.actions;

import ua.com.epam.configurations.PropertiesManager;
import ua.com.epam.factory.DriverProvider;

public class NavigationActions {

    public void navigateToLoginPage() {
        DriverProvider.getDriver().get(PropertiesManager.getBaseUrl());
    }
}
