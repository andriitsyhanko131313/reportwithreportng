package ua.com.epam.ui.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import ua.com.epam.decorator.elements.Button;
import ua.com.epam.factory.DriverProvider;

public class DraftsPage extends AbstractPage {
    private static final Logger logger = LogManager.getLogger(DraftsPage.class);

    public Button getMessageBySubject(String subject) {
        logger.info("Open message by subject");
        By locator = By.xpath(String.format("//div[@role='link']//span[text()='%s']/ancestor::tr", subject));
        return new Button(DriverProvider.getDriver().findElement(locator), locator);
    }
}
