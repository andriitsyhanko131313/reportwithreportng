package ua.com.epam.utils;


import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Reporter;
import ua.com.epam.factory.DriverProvider;

import java.io.File;
import java.io.IOException;

public class ReportAttachments {

    public static void screenCapture() {
        System.setProperty("org.uncommons.reportng.escape-output", "false");
        byte[] scrFile = ((TakesScreenshot) DriverProvider.getDriver()).getScreenshotAs(OutputType.BYTES);
        String path = "screenshots/";
        File screenshotName = new File(path + DriverProvider.getDriver().getTitle() + ".jpg");
        try {
            FileUtils.writeByteArrayToFile(screenshotName, scrFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Reporter.log("<br>  <img src='" + screenshotName + "' height='500' width='500' /><br>");
        Reporter.log("<a href=" + screenshotName + "></a>");
    }
}