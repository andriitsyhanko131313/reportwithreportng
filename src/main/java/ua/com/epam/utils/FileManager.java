package ua.com.epam.utils;

import ua.com.epam.configurations.PropertiesManager;


public class FileManager {
    public static User getUser() {
        return FileReader.readObject(PropertiesManager.getUsersFilePath(), User.class);
    }

    public static Letter getLetter() {
        return FileReader.readObject(PropertiesManager.getLetterFilePath(), Letter.class);
    }

    private FileManager() {
    }
}
